package com.redwinter.logkit.sdk.annotation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportAware;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author goudadong
 * @description enabled log
 * @datetime 2021-12-14 14:12
 * @email 2360564660@qq.com
 **/
@Configuration
@Slf4j
public abstract class AbstractLogConfiguration implements ImportAware {
    AnnotationAttributes enableLogRecord;

    @Override
    public void setImportMetadata(AnnotationMetadata importMetadata) {
        this.enableLogRecord = AnnotationAttributes.fromMap(
                importMetadata.getAnnotationAttributes(EnabledBizLogRecord.class.getName(), false));
        if (this.enableLogRecord == null) {
            log.info("@EnabledLogRecord is not present on importing class");
        }
    }
}
