package com.redwinter.logkit.test.service;

import com.redwinter.logkit.sdk.annotation.BizLogRecord;
import com.redwinter.logkit.sdk.context.LogSpELVarContext;
import com.redwinter.logkit.test.beans.User;
import org.springframework.stereotype.Service;

/**
 * @author goudadong
 * @description 测试
 * @datetime 2021-12-14 21:51
 * @email 2360564660@qq.com
 **/
@Service
public class LogTestImpl implements LogTest {


    @Override
    @BizLogRecord(data = "{#u}", bizType = "order", condition = "#user.name eq '张三'")
    public void test(User user) {
        System.out.println("nnn");
        LogSpELVarContext.putVariables("u", user);
    }
}
