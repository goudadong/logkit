package com.redwinter.logkit.sdk.parser;

import lombok.Data;

import java.lang.reflect.Method;

/**
 * @author goudadong
 * @description 日志计算 SpEL 表达式计算根对象
 * @datetime 2021-12-14 15:45
 * @email 2360564660@qq.com
 **/
@Data
public class LogExpressionRootObject {

    private final Method method;

    private final Object[] args;

    private final Object target;

    private final Class<?> targetClass;
}
