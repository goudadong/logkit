package com.redwinter.logkit.sdk.annotation;

import com.redwinter.logkit.sdk.aop.BeanFactoryLogAdvisor;
import com.redwinter.logkit.sdk.parser.AnnotationLogOperationSource;
import com.redwinter.logkit.sdk.parser.LogOperationSource;
import com.redwinter.logkit.sdk.service.LogRecordService;
import com.redwinter.logkit.sdk.service.impl.DefaultLogRecordServiceImpl;
import com.redwinter.logkit.sdk.support.LogInterceptor;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Role;

/**
 * @author goudadong
 * @description jdk动态代理配置
 * @datetime 2021-12-14 14:05
 * @email 2360564660@qq.com
 **/
@Configuration
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
public class ProxyLogConfiguration extends AbstractLogConfiguration {

    @Bean
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    public BeanFactoryLogAdvisor logAdvisor(LogRecordService logRecordService) {
        BeanFactoryLogAdvisor advisor = new BeanFactoryLogAdvisor();
        advisor.setLogRecordOperationSource(logRecordOperationSource());
        advisor.setAdvice(logInterceptor(logRecordService));
        return advisor;
    }

    @Bean
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    public LogOperationSource logRecordOperationSource() {
        return new AnnotationLogOperationSource();
    }

    @Bean
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    public LogInterceptor logInterceptor(LogRecordService logRecordService) {
        LogInterceptor logInterceptor = new LogInterceptor(logRecordService);
        logInterceptor.setLogRecordOperationSource(logRecordOperationSource());
        return logInterceptor;
    }

    @Bean
    @ConditionalOnMissingBean(LogRecordService.class)
    public LogRecordService logRecordService() {
        return new DefaultLogRecordServiceImpl();
    }
}
