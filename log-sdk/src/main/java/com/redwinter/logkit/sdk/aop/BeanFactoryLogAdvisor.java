package com.redwinter.logkit.sdk.aop;

import com.redwinter.logkit.sdk.parser.LogOperationSource;
import org.springframework.aop.ClassFilter;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractBeanFactoryPointcutAdvisor;

/**
 * @author goudadong
 * @description 日志通知
 * @datetime 2021-12-14 17:18
 * @email 2360564660@qq.com
 **/
public class BeanFactoryLogAdvisor extends AbstractBeanFactoryPointcutAdvisor {

    private LogOperationSource logRecordOperationSource;
    private final LogOperationSourcePointcut pointcut;

    public BeanFactoryLogAdvisor() {
        this.pointcut = new LogOperationSourcePointcut();
    }

    public void setClassFilter(ClassFilter classFilter) {
        this.pointcut.setClassFilter(classFilter);
    }

    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }

    public void setLogRecordOperationSource(LogOperationSource logRecordOperationSource) {
        this.pointcut.setLogRecordOperationSource(logRecordOperationSource);
    }
}
