package com.redwinter.logkit.sdk.service;

import com.redwinter.logkit.sdk.beans.LogResult;

/**
 * @author goudadong
 * @description 保存数据
 * @datetime 2021-12-15 17:35
 * @email 2360564660@qq.com
 **/
public interface LogRecordService {

    void apply(LogResult recordResult);
}
