package com.redwinter.logkit.sdk.parser;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author goudadong
 * @description 日志操作
 * @datetime 2021-12-14 14:32
 * @email 2360564660@qq.com
 **/
@Data
@Accessors(chain = true)
public class LogOperation {
    /**
     * 注解名称
     */
    private final String name;
    /**
     * 业务类型
     */
    private final String bizType;

    /**
     * 日志数据  json字符串
     */
    private final String data;

    /**
     * 条件，满足了就记录日志，默认是要记录日志的
     */
    private final String condition;

}
