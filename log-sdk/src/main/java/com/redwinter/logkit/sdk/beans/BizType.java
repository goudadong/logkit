package com.redwinter.logkit.sdk.beans;

/**
 * @author goudadong
 * @description 业务类型
 * @datetime 2021-12-15 16:45
 * @email 2360564660@qq.com
 **/
public class BizType {

    public static final String TASK = "task";
    public static final String EQUIP = "equip";
    public static final String ORDER = "order";
    public static final String DEFAULT = "default";

}
