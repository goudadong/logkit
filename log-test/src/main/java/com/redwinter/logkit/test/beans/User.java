package com.redwinter.logkit.test.beans;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author goudadong
 * @description user
 * @datetime 2021-12-15 11:47
 * @email 2360564660@qq.com
 **/
@Data
@Accessors(chain = true)
public class User implements Serializable {
    private String name;
    private String password;
}