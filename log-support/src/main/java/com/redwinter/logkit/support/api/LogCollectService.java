package com.redwinter.logkit.support.api;

/**
 * @author goudadong
 * @description 日志收集
 * @datetime 2021-12-16 15:44
 * @email 2360564660@qq.com
 **/
public interface LogCollectService {

    void store(String logType, String logMsg);

}
