package com.redwinter.logkit.sdk.parser;

import org.springframework.lang.Nullable;

import java.lang.reflect.Method;
import java.util.Collection;

/**
 * @author goudadong
 * @description 日志解析
 * @datetime 2021-12-14 14:29
 * @email 2360564660@qq.com
 **/
public interface LogParser {

    /**
     * 基于注释类型解析指定方法的日志定义
     */
    @Nullable
    Collection<LogOperation> parseLogRecordAnnotations(Method method);
}
