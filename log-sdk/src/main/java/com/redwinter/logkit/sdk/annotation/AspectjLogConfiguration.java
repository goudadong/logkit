package com.redwinter.logkit.sdk.annotation;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Role;

/**
 * @author goudadong
 * @description aspectj cglib 代理
 * @datetime 2021-12-14 14:16
 * @email 2360564660@qq.com
 **/
@Configuration
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
public class AspectjLogConfiguration extends AbstractLogConfiguration {
}
