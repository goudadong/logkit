package com.redwinter.logkit.support.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author goudadong
 * @description 日志属性配置
 * @datetime 2021-12-16 17:12
 * @email 2360564660@qq.com
 **/
@ConfigurationProperties(prefix = "biz-log")
@Data
public class LogProperties {
    /**
     * 日志存储位置
     */
    private String filePath = "./logs/";
    /**
     * 日志文件最大大小
     */
    private String maxFileSize = "128MB";
    /**
     * 最大历史记录
     */
    private int maxHistory = 15;
    /**
     * 总大小限制
     */
    private String totalSizeCap = "4GB";
}
