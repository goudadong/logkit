package com.redwinter.logkit.sdk.parser;

import com.redwinter.logkit.sdk.context.LogSpELVarContext;
import org.springframework.context.expression.MethodBasedEvaluationContext;
import org.springframework.core.ParameterNameDiscoverer;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * @author goudadong
 * @description 日志计算上下文，自动将方法参数添加为计算变量
 * @datetime 2021-12-14 15:50
 * @email 2360564660@qq.com
 **/
public class LogEvaluationContext extends MethodBasedEvaluationContext {

    public LogEvaluationContext(Object rootObject,
                                Method method,
                                Object[] arguments,
                                ParameterNameDiscoverer parameterNameDiscoverer,
                                Object ret,
                                String errorMsg) {
        super(rootObject, method, arguments, parameterNameDiscoverer);
        Map<String, Object> variables = LogSpELVarContext.getVariables();
        variables.forEach(this::setVariable);
        setVariable("_ret",ret);
        setVariable("_errorMsg",errorMsg);
    }
}
