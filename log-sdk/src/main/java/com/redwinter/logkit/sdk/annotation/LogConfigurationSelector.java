package com.redwinter.logkit.sdk.annotation;

import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.AdviceModeImportSelector;

/**
 * @author goudadong
 * @description 日志记录选择
 * @datetime 2021-12-14 13:59
 * @email 2360564660@qq.com
 **/
public class LogConfigurationSelector extends AdviceModeImportSelector<EnabledBizLogRecord> {

    private final static String ASPECTJ_LOG_RECORD_CONFIGURATION = "com.redwinter.logkit.sdk.annotation.AspectjLogRecordConfiguration";

    @Override
    protected String[] selectImports(AdviceMode adviceMode) {
        switch (adviceMode) {
            case PROXY:
                return new String[]{ProxyLogConfiguration.class.getName()};
            case ASPECTJ:
                return new String[]{ASPECTJ_LOG_RECORD_CONFIGURATION};
            default:
                return null;
        }
    }
}
