package com.redwinter.logkit.test.controller;

import com.redwinter.logkit.test.beans.User;
import com.redwinter.logkit.test.service.LogTest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author goudadong
 * @description 测试
 * @datetime 2021-12-21 19:56
 * @email 2360564660@qq.com
 **/
@RestController
@RequestMapping
public class TestController {

    @Resource
    public LogTest logTest;

    @GetMapping("/test")
    public String test() {
        User user = new User();
        user.setName("张三");
        user.setPassword("123456");
        logTest.test(user);
        return "success";
    }
}
