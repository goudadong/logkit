package com.redwinter.logkit.sdk.parser;

import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;

/**
 * @author goudadong
 * @description 日志注解SpEL 表达式计算器
 * @datetime 2021-12-14 16:00
 * @email 2360564660@qq.com
 **/
public class LogExpressionEvaluator {

    private final SpelExpressionParser parser;

    private final ParameterNameDiscoverer parameterNameDiscoverer = new DefaultParameterNameDiscoverer();

    public ParameterNameDiscoverer getParameterNameDiscoverer() {
        return parameterNameDiscoverer;
    }

    public LogExpressionEvaluator(SpelExpressionParser parser) {
        this.parser = parser;
    }

    public LogExpressionEvaluator() {
        this(new SpelExpressionParser());
    }

    protected Expression getExpression(String expression){
        return parser.parseExpression(expression);
    }
}
