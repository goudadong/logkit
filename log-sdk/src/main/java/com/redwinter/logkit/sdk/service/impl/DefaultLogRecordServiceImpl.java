package com.redwinter.logkit.sdk.service.impl;

import com.redwinter.logkit.sdk.beans.LogResult;
import com.redwinter.logkit.sdk.service.LogRecordService;
import com.redwinter.logkit.sdk.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author goudadong
 * @description 默认的日志处理方式
 * @datetime 2021-12-15 17:38
 * @email 2360564660@qq.com
 **/
@Slf4j
public class DefaultLogRecordServiceImpl implements LogRecordService {
    @Override
    public void apply(LogResult recordResult) {
        log.info(JsonUtil.toJson(recordResult));
    }
}
