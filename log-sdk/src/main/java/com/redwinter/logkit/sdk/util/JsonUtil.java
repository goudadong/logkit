package com.redwinter.logkit.sdk.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author goudadong
 * @description json工具类
 * @datetime 2021-12-15 19:24
 * @email 2360564660@qq.com
 **/
public class JsonUtil {
    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    private static final Gson gson;

    static {
        gson = new GsonBuilder()
                .setDateFormat(DATE_FORMAT)
                .serializeNulls()
                .create();

    }

    public static String toJson(Object src) {
        return gson.toJson(src);
    }
}
