package com.redwinter.logkit.sdk.annotation;

import com.redwinter.logkit.sdk.beans.BizType;
import com.redwinter.logkit.sdk.beans.LogResult;

import java.lang.annotation.*;

/**
 * @author goudadong
 * @description 日志记录注解
 * @datetime 2021-12-14 11:46
 * @email 2360564660@qq.com
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface BizLogRecord {

    /**
     * 业务类型
     */
    String bizType() default BizType.DEFAULT;

    /**
     * 日志数据
     */
    String data();

    /**
     * 条件，满足了就记录 {@code data}日志数据，默认是要记录日志的
     * 不满足条件的时候只会将 {@link LogResult#getData()} 设置为null
     */
    String condition() default "";


}
