package com.redwinter.logkit.sdk.parser;

import org.springframework.aop.support.AopUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;

/**
 * @author goudadong
 * @description 获取日志操作
 * @datetime 2021-12-14 15:16
 * @email 2360564660@qq.com
 **/
public abstract class AbstractFallbackLogOperationSource implements LogOperationSource {

    @Override
    public Collection<LogOperation> getLogOperations(Method method, Class<?> targetClass) {
        return computeCacheOperations(method, targetClass);
    }

    protected Collection<LogOperation> computeCacheOperations(Method method, Class<?> targetClass) {
        // Don't allow no-public methods as required.
        if (!Modifier.isPublic(method.getModifiers())) {
            return null;
        }

        // The method may be on an interface, but we need attributes from the target class.
        // If the target class is null, the method will be unchanged.
        Method specificMethod = AopUtils.getMostSpecificMethod(method, targetClass);
        return findCacheOperations(specificMethod);
    }

    protected abstract Collection<LogOperation> findCacheOperations(Method specificMethod);
}
