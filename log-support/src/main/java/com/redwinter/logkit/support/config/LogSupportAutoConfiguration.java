package com.redwinter.logkit.support.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author goudadong
 * @description 日志自动配置
 * @datetime 2021-12-21 13:46
 * @email 2360564660@qq.com
 **/
@Configuration
@Import(LogSupportConfig.class)
@EnableConfigurationProperties(LogProperties.class)
public class LogSupportAutoConfiguration {

}
