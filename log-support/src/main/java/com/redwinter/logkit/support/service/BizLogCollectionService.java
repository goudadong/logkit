package com.redwinter.logkit.support.service;

import ch.qos.logback.classic.Logger;
import com.redwinter.logkit.support.config.LogProperties;

/**
 * @author goudadong
 * @description 业务日志服务实现
 * @datetime 2021-12-16 21:54
 * @email 2360564660@qq.com
 **/
public class BizLogCollectionService extends AbstractLogCollect {
    /**
     * 索引前缀
     */
    private static final String BIZ_FILE_PREFIX = "biz_";

    public BizLogCollectionService(LogProperties logProperties) {
        super(logProperties);
    }

    @Override
    public void store(String logType, String logMsg) {
        Logger logger = getLogger(BIZ_FILE_PREFIX + logType);
        logger.info(logMsg);
    }
}
