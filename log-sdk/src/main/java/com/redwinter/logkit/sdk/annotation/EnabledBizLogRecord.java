package com.redwinter.logkit.sdk.annotation;

import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author goudadong
 * @description 开启日志记录
 * @datetime 2021-12-14 13:52
 * @email 2360564660@qq.com
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(LogConfigurationSelector.class)
public @interface EnabledBizLogRecord {

    /**
     * 代理模式，默认使用jdk代理
     */
    AdviceMode mode() default AdviceMode.PROXY;
}
