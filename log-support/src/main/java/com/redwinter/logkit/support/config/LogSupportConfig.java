package com.redwinter.logkit.support.config;

import com.redwinter.logkit.support.api.LogCollectService;
import com.redwinter.logkit.support.service.BizLogCollectionService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author goudadong
 * @description 日志支持配置
 * @datetime 2021-12-21 14:07
 * @email 2360564660@qq.com
 **/
@Configuration
@ComponentScan(basePackageClasses = {com.redwinter.logkit.support.service.LogRecordJsonService.class} )
public class LogSupportConfig {

    @Bean
    @ConditionalOnMissingBean(LogCollectService.class)
    public LogCollectService logCollectService(LogProperties properties) {
        return new BizLogCollectionService(properties);
    }
}
