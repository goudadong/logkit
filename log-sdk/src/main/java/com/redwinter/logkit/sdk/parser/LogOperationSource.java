package com.redwinter.logkit.sdk.parser;

import org.springframework.lang.Nullable;

import java.lang.reflect.Method;
import java.util.Collection;

/**
 * @author goudadong
 * @description 日志操作源
 * @datetime 2021-12-14 15:12
 * @email 2360564660@qq.com
 **/
public interface LogOperationSource {

    @Nullable
    Collection<LogOperation> getLogOperations(Method method, @Nullable Class<?> targetClass);
}
