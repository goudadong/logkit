package com.redwinter.logkit.sdk.aop;

import com.redwinter.logkit.sdk.parser.LogOperationSource;
import org.springframework.aop.ClassFilter;
import org.springframework.aop.support.StaticMethodMatcherPointcut;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.lang.reflect.Method;

/**
 * @author goudadong
 * @description 日志抽象切点
 * @datetime 2021-12-14 17:11
 * @email 2360564660@qq.com
 **/
public class LogOperationSourcePointcut extends StaticMethodMatcherPointcut implements Serializable {

    private LogOperationSource logRecordOperationSource;

    @Override
    public boolean matches(Method method, Class<?> targetClass) {
        return !CollectionUtils.isEmpty(logRecordOperationSource.getLogOperations(method, targetClass));
    }

    @Override
    public ClassFilter getClassFilter() {
        return super.getClassFilter();
    }

    public void setLogRecordOperationSource(LogOperationSource logRecordOperationSource) {
        this.logRecordOperationSource = logRecordOperationSource;
    }
}
