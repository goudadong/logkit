package com.redwinter.logkit.test;

import com.redwinter.logkit.sdk.annotation.EnabledBizLogRecord;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author goudadong
 * @description 引导类
 * @datetime 2021-12-21 13:26
 * @email 2360564660@qq.com
 **/
@SpringBootApplication
@EnabledBizLogRecord
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
