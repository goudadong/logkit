package com.redwinter.logkit.test.service;

import com.redwinter.logkit.test.beans.User;

import java.io.Serializable;

/**
 * @author goudadong
 * @description todo
 * @datetime 2021-12-14 21:57
 * @email 2360564660@qq.com
 **/
public interface LogTest extends Serializable {
    void test(User user);
}
