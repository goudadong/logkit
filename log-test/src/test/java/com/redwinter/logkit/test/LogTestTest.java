package com.redwinter.logkit.test;

import com.redwinter.logkit.test.beans.User;
import com.redwinter.logkit.test.service.LogTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author goudadong
 * @description 测试
 * @datetime 2021-12-14 21:56
 * @email 2360564660@qq.com
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class LogTestTest {

    @Autowired
    LogTest logTest;

    @Test
    public void testLog() {
        User user = new User();
        user.setName("张三").setPassword("123456");
        logTest.test(user);
    }


    @Test
    public void testCheckHashSpEL() {
        String pre = "([a-zA-Z]+[\\w]*)";
        String exp = "^(:?\\{#" + pre + "(\\.)?" + pre + "?})$" + "|^#" + pre + "(\\.)?" + pre + "?$";
        Pattern pattern = Pattern.compile(exp);
        Matcher matcher = pattern.matcher("{#u.name}");
        System.out.println(matcher.matches());

        System.out.println(checkHasSpEL("{ss#}"));
        System.out.println(checkHasSpEL("{#ss#}"));
        System.out.println("{#u.name}".replaceFirst("#[a-zA-Z]+","#ssss"));
        System.out.println(matcher.group());
    }

    private boolean checkHasSpEL(String expression) {
        return (expression.startsWith("{")
                && expression.endsWith("}")
                && expression.indexOf("#") == 1)
                || expression.startsWith("#");

    }

}