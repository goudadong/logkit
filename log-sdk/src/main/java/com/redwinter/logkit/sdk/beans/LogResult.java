package com.redwinter.logkit.sdk.beans;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author goudadong
 * @description 解析出来的数据结果
 * @datetime 2021-12-15 15:59
 * @email 2360564660@qq.com
 **/
@Data
@Accessors(chain = true)
public class LogResult {
    /**
     * 模块代码
     */
    private String moduleCode;
    /**
     * 模块名称
     */
    private String moduleName;
    /**
     * 保存的数据
     */
    private Object data = null;
    /**
     * 异常信息
     */
    private String errorMsg;
    /**
     * 执行的目标类
     */
    private String targetClass;
    /**
     * 执行的目标方法
     */
    private String targetMethod;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 方法消耗时间
     */
    private Long spendTime;
    /**
     * 方法是否调用成功
     */
    private Boolean flag;
    /**
     * 业务类型
     */
    private String bizType;
}
