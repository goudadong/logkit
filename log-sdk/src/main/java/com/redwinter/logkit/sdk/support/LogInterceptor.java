package com.redwinter.logkit.sdk.support;

import com.redwinter.logkit.sdk.context.LogSpELVarContext;
import com.redwinter.logkit.sdk.service.LogRecordService;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import java.io.Serializable;

/**
 * @author goudadong
 * @description 日志拦截器
 * @datetime 2021-12-14 21:38
 * @email 2360564660@qq.com
 **/
public class LogInterceptor extends LogAspectSupport implements MethodInterceptor, Serializable {

    public LogInterceptor(LogRecordService logRecordService) {
        super(logRecordService);
    }

    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        try {
            return execute(methodInvocation, methodInvocation.getThis(), methodInvocation.getMethod(), methodInvocation.getArguments());
        } finally {
            LogSpELVarContext.clear();
        }
    }
}
