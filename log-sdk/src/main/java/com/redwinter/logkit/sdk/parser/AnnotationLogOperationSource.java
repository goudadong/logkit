package com.redwinter.logkit.sdk.parser;

import java.lang.reflect.Method;
import java.util.Collection;

/**
 * @author goudadong
 * @description 解析日志注解
 * @datetime 2021-12-14 15:26
 * @email 2360564660@qq.com
 **/
public class AnnotationLogOperationSource extends AbstractFallbackLogOperationSource {
    @Override
    protected Collection<LogOperation> findCacheOperations(Method method) {
        return determineCacheOperations(parser -> parser.parseLogRecordAnnotations(method));
    }

    private Collection<LogOperation> determineCacheOperations(LogOperationProvider provider) {
        return provider.getAnnotationOperation(new SpringLogAnnotationParser());
    }

    @FunctionalInterface
    protected interface LogOperationProvider {
        Collection<LogOperation> getAnnotationOperation(LogParser parser);
    }
}
